# Nom du fichier final
TITRE      := "Le_C_en_20h"

# Inclusion conditionnelle de la couverture
COVER      := src/cover.png
COVER_FILE := $(wildcard $(COVER)) # "src/cover.png" s’il est présent, une variable vide sinon
ifneq ($(COVER_FILE),)
	DRAFT_PANDOC_OPTIONS := --epub-cover-image=$(COVER)
endif

# Liste des fichiers à remplacer
REPLACED_FILES := $(wildcard work/replaced_files/*)

# Liste des fichiers à effacer
DELETED_FILES := $(shell cat work/delete_list.txt)

all: firstpass regexp epub

all+pdf: all pdf

allfromepub: dezip regexp epub

firstpass: src
	@ rm -Rf deflate/*
	@ mkdir --parents tmp
	@ pandoc src/*.md $(DRAFT_PANDOC_OPTIONS) -o tmp/draft1.epub
	@ unzip tmp/draft1.epub -d deflate
	@ rm tmp/draft1.epub

dezip: src
	@ rm -Rf deflate/mimetype deflate/META-INF/ deflate/EPUB/ deflate/OEBPS/
	@ unzip src/*.epub -d deflate

epub: work
	@ mkdir --parents build
	@ cd tmp/epub && zip -rX ../../build/$(TITRE).epub *

work: deflate/
	@ mkdir --parents tmp/epub
	@ cp -r deflate/* tmp/epub
ifneq ($(REPLACED_FILES),)
	@ cp -r work/replaced_files/* tmp/epub
endif
	@ for f in $(DELETED_FILES) ; do rm -f "tmp/epub/$${f}" ; done

regexp: deflate/
# ajouter «-name "*.txt" -o -name "*.md"» si on veut préciser les extensions des fichiers à modifier
	@ find deflate/* -type f -exec sed -f "$$(readlink -f -- "scripts/regexp.sed")" --in-place {} \;

validate: build/$(TITRE).epub
	@ - epubcheck -j build/epubcheck_report.json build/monepub_final.epub # Le - est nécessaire en début de commande make pour que make ne s’arrête pas si il reçoit une erreur de cette commande
	@ ace --force --outdir build/ace_reports build/$(TITRE).epub

latex: src/*.md templates/*
	@ mkdir --parents tex
	@ pandoc -s src/*.md --template templates/book.latex -o tex/$(TITRE).tex

pdf: latex
	@ - xelatex -output-directory tex tex/$(TITRE).tex # Le - est nécessaire en début de commande make pour que make ne s’arrête pas si il reçoit une erreur de cette commande
	@ cp tex/$(TITRE).pdf build/$(TITRE).pdf

clean:
	@ rm -Rf deflate
	@ rm -Rf build
	@ rm -Rf tmp


