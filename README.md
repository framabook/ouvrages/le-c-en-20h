Ce dépôt est basé sur l’utilisation du [Système de publication Framabook](https://framagit.org/framabook/systeme-publication-framabook).

Titre :

- Le C en 20h

Auteurs : 

- Éric Berthomier
- Daniel Schang

Éditeurice :

- ?

Publicateurice :

- Première édition :
  - Nadège Dauvergne (couverture originelle)
  - [La Poule ou l’Œuf](http://www.pouleouoeuf.org/)
- Seconde édition :
  - Yann Kervran

Dates de sorties :

- première édition : septembre 2011
- seconde édition : juin 2021

ISBN :

- première édition :
- seconde édition : 9791092674354

Licence de l’ouvrage :

- [Creative Commons BY SA](https://creativecommons.org/licenses/by-sa/2.0/fr/)


